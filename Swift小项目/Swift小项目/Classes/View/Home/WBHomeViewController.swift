//
//  WBHomeViewController.swift
//  Swift小项目
//
//  Created by bddzzw on 2017/10/26.
//  Copyright © 2017年 bddzzw. All rights reserved.
//

import UIKit
private  let cellID = "cellID"
class WBHomeViewController: WBBaseViewController {
    
   private lazy var arrayM = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }

    @objc func leftItemBtnClick() {
        let vc = WBDemoViewController()
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func loadData() {
        //模拟耗时操作
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            for i in 0..<10 {
                if self.isPullup {
                   self.arrayM.append("上拉"+i.description)
                }else{
                    self.arrayM.append("下拉"+i.description)
                  
                }
                
            }
              self.isPullup = false
            //结束刷新 回到主线程之后就要做这些事情
            
            self.refreshControl?.endRefreshing()
            //更新数据
            self.myTableView?.reloadData()
        }
    }

    // MARK: - 设置UI
    override func setupMyTableView() {
        super.setupMyTableView()
//        let btn = UIButton.cz_textButton("好友", fontSize: 16, normalColor: UIColor.gray, highlightedColor: UIColor.orange)
//        btn?.addTarget(self, action: #selector(leftItemBtnClick), for: .touchUpInside)
//        navigationItem.leftBarButtonItem=UIBarButtonItem(customView: btn!)
        //以下是自己封装的方法
        navigationItem.leftBarButtonItem=UIBarButtonItem(title: "好友", fontSize: 16, target: self, action: #selector(leftItemBtnClick))
        //注册原型cell
        myTableView?.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
        
    }
    
}

// MARK: - Declarations in extensions cannot override yet


extension WBHomeViewController{
   
    
}

// MARK: - 重写数据源方法
extension WBHomeViewController{
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayM.count
        
        
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = myTableView?.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        cell?.textLabel?.text=arrayM[indexPath.row] as? String
        
        
        return cell!
        
    }
}
