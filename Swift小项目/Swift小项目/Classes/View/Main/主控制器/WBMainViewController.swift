//
//  WBMainViewController.swift
//  Swift小项目
//
//  Created by bddzzw on 2017/10/26.
//  Copyright © 2017年 bddzzw. All rights reserved.
//

import UIKit

class WBMainViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupChildControllers()
        setupComposeButton()
        
    }
    
    /// 使用代码控制设备的方向 可以在需要横竖的时候单独处理
    
    /// 设置支持的方向之后 当前的控制器及自控制器都会遵循这个方向
    
    /// 如果是播放视频 通常是通过modal 展示的
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .portrait
    }
    /// 待完善
    /// private 保证方法被保护
    /// @objc 允许函数运行时通过oc的机制访问
    @objc private func composeBtnClick(){
        print("中间Tabbar的点击方法")
        //MARk 测试方向的旋转
        let vc = UIViewController()
        
        vc.view.backgroundColor=UIColor.cz_random()
        
        let nav  = UINavigationController(rootViewController: vc)
        
        present(nav, animated: true, completion: nil)
        
        
        
    }
    /// Declaration 声明 is only valid有效 at file scop 类的外边
    private lazy var composeButton:UIButton=UIButton.cz_imageButton("tabbar_compose_icon_add", backgroundImageName: "tabbar_compose_button")
    
}

// MARK: - extension 在Swift中可以切分代码块 可以将相近的功能函数放在一个extension中 便于维护代码

// MARK: - 页面设置
extension WBMainViewController{
    
    
    private func setupComposeButton(){
        tabBar.addSubview(composeButton)
        
        //计算按钮的宽度
        let count  = CGFloat(childViewControllers.count)
        //-1 是为了减少盲区 容错点
        let w  = tabBar.bounds.width / count - 1
        composeButton.frame=tabBar.bounds.insetBy(dx: 2 * w, dy: -5)
        
        composeButton .addTarget(self, action: #selector(composeBtnClick), for: .touchUpInside)
        
    }
    
    private func setupChildControllers(){
        //从json中拿到数据
        guard let path  = Bundle.main.path(forResource: "test.json", ofType: nil),
        let data  = NSData(contentsOfFile: path),
        let array  = try? JSONSerialization.jsonObject(with: data as Data, options: []) as? [[String:AnyObject]]
        else {
            return
        }
        
        
        
        
        
        
//        let array :[[String:AnyObject]] = [
//            ["clsName":"WBHomeViewController" as AnyObject,"title":"首页" as AnyObject,"imageName":"home" as AnyObject,"visitorInfo":["imageName":"","message":"美好的一天从今天的登录开始==>美好的一天从今天的登录开始"] as AnyObject
//            ],
//            ["clsName":"WBMessageController" as AnyObject,"title":"消息" as AnyObject,"imageName":"message_center" as AnyObject,"visitorInfo":["imageName":"visitordiscover_image_message","message":"消息消息不看后悔一辈子，我的至尊大侠登陆后才能查看"] as AnyObject
//            ],
//            ["clsName":"UIViewController" as AnyObject],
//
//            ["clsName":"WBDiscoverController" as AnyObject,"title":"发现" as AnyObject,"imageName":"discover" as AnyObject,"visitorInfo":["imageName":"visitordiscover_image_message","message":"这是发现发现发现这是发现发现发现这是发现发现发现这是发现发现发现我的至尊大侠登陆后才能查看"] as AnyObject
//            ],
//            ["clsName":"WBProfileController" as AnyObject,"title":"我的" as AnyObject,"imageName":"profile" as AnyObject,"visitorInfo":["imageName":"visitordiscover_image_profile","message":"我的至尊大侠登陆后才能查看"] as AnyObject
//            ]
//        ]
//
        //写出plist文件
//        (array as NSArray).write(toFile: "/Users/wangpeijie/Desktop/11/test.plist", atomically: true)
        
        //数据->json 序列化
//        let data = try! JSONSerialization.data(withJSONObject: array, options: [])
//        (data as NSData).write(toFile: "/Users/wangpeijie/Desktop/11/test.json", atomically: true)
//
        
        var arrayM = [UIViewController]()
        for dict in array! {
            arrayM.append(controller(dict: dict))
        }
        
        viewControllers=arrayM
    }
    
    
    
    /// 使用字典创建一个自控制器
    ///
    /// - Parameter dict: 信息字典 包含类型 控制器名 图片名
    /// - Returns: 返回自控制器
    private func controller(dict:[String:AnyObject])->UIViewController{
        guard let clsName=dict["clsName"] as? String,
            let title=dict["title"] as? String,
            let imageName=dict["imageName"] as? String,
            let  cls=NSClassFromString(Bundle.main.namespace + "." + clsName) as? WBBaseViewController.Type,
           let visitorDict = dict["visitorInfo"] as? [String:String] else {
                return UIViewController()
        }
        //创建控制器
        let vc = cls.init()
        vc.title=title
        
        
        vc.visitorInfor=visitorDict
        vc.tabBarItem.image=UIImage(named:"tabbar_"+imageName)
   
        vc.tabBarItem.setTitleTextAttributes(
            [NSAttributedStringKey.foregroundColor:UIColor.orange],
            for: .highlighted)
       
       
       //设置tabbar 文字的大小和颜色
        vc.tabBarItem.setTitleTextAttributes(
            [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 16)],
            for: .normal)
        vc.tabBarItem.selectedImage=UIImage(named:"tabbar_"+imageName+"_selected")?.withRenderingMode(.alwaysOriginal)
        
        let nav = WBNavationController(rootViewController:vc)
        
        return nav
        
        
    }
}
