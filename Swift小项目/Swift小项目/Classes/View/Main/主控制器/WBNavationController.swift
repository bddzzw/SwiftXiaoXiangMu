//
//  WBNavationController.swift
//  Swift小项目
//
//  Created by bddzzw on 2017/10/26.
//  Copyright © 2017年 bddzzw. All rights reserved.
//

import UIKit

class WBNavationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        navigationBar.isHidden=true
        // Do any additional setup after loading the view.
    }
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        //如果不是栈底控制器才需要隐藏 本控制器不需要处理
        
        
        if childViewControllers.count>0 {
            viewController.hidesBottomBarWhenPushed=true
            
            if let vc=viewController as? WBBaseViewController {
                var title = "返回"
                if childViewControllers.count==1{
                     title = childViewControllers.first?.title ?? "返回"
                }
               
                vc.navigationItem.leftBarButtonItem=UIBarButtonItem(title: title, fontSize: 16, target: self, action: #selector(popToParent),isBack:true)
            }
           
            
        }
        
        super.pushViewController(viewController, animated: animated)
       
    }
    @objc private func popToParent(){
        popViewController(animated: true)
    }
   
}
