//
//  WBVisitorView.swift
//  Swift小项目
//
//  Created by bddzzw on 2017/10/29.
//  Copyright © 2017年 bddzzw. All rights reserved.
//

import UIKit

//访客视图
class WBVisitorView: UIView {

    //注册按钮
      var registerBtn :UIButton = UIButton.cz_textButton(
        "注册",
        fontSize: 16, normalColor: UIColor.orange,
        highlightedColor: UIColor.black,
        backgroundImageName: "common_button_white_disable")
    //登录按钮
      var loginBtn :UIButton = UIButton.cz_textButton("登录",
                                                                 fontSize: 16,
                                                                 normalColor: UIColor.black, highlightedColor: UIColor.orange,backgroundImageName: "common_button_white_disable")
    
    
    ///访客视图的信息字典[imageName / message]
    ///如果是首页 ImageName==“”
    var visitorInfo: [String : String]?{
        didSet{
            //取字典信息
            guard let imageName=visitorInfo?["imageName"],
                let message = visitorInfo?["message"] else{
                    return
            }
            //设置消息
            tipLabel.text=message
            //设置图像 首页不需要
            if imageName == "" {
                startAnimation()
                return
            }
            iconView.image=UIImage(named:imageName)
            //其他访客视图不显示小房子和遮罩层
            houseView.isHidden=true
            maskIconView.isHidden=true
            
            
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //旋转动画
    //'M_PI' is deprecated: Please use 'Double.pi' or '.pi' to get the value of correct type and avoid casting.
    private func startAnimation(){
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.toValue = 2.0 * Double.pi
        anim.repeatCount = MAXFLOAT
        anim.duration=15
        //动画完成不删除  ifIconView 被释放动画会一起释放 这个设置连续播放的动画
        
        anim.isRemovedOnCompletion=false
        //动画添加到图层
        iconView.layer.add(anim, forKey: nil)
        
        
    }
    //显示空间
    //图像
    private lazy var iconView :UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    //遮罩层
    private lazy var maskIconView : UIImageView = UIImageView(image:UIImage(named: "visitordiscover_feed_mask_smallicon"))
    //房子
    private lazy var houseView : UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    //提示标签
    private lazy var tipLabel : UILabel = UILabel.cz_label(
        withText: "美好的一天从今天的登录开始==>美好的一天从今天的登录开始",
        fontSize: 15,
        color: UIColor.darkGray)
   
    
    
    
}

// MARK: - extension 中不能有属性
extension WBVisitorView{
    func setupUI (){
        self.backgroundColor=UIColor.cz_color(withHex: 0xEDEDED)
        self.addSubview(iconView)
        self.addSubview(maskIconView)
        self.addSubview(houseView)
        self.addSubview(tipLabel)
        self.addSubview(registerBtn)
        self.addSubview(loginBtn)
        //取消 autoresizing
        for v in subviews {
            v.translatesAutoresizingMaskIntoConstraints = false
        }
        //自动布局
        //MARK:item<某个视图>attribute<适配属性>relatedBy<小于等于>toItem<另一个控件>attribute<参照物属性>multiplier<乘积>constant<常数>
       //圆圈图片适配
        addConstraint(NSLayoutConstraint(item: iconView,
                                         attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView,
                                         attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: -30))
        //房子图片适配
        addConstraint(NSLayoutConstraint(item: houseView,
                                         attribute: .centerX, relatedBy: .equal, toItem: iconView, attribute: .centerX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: houseView,
                                         attribute: .centerY, relatedBy: .equal, toItem: iconView, attribute: .centerY, multiplier: 1.0, constant: 0))
        //提示语适配
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .centerX, relatedBy: .equal, toItem: iconView, attribute: .centerX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute: .bottom, relatedBy: .equal, toItem: iconView, attribute: .bottom, multiplier: 1.0, constant: 30))
        addConstraint(NSLayoutConstraint(item: tipLabel,
                                         attribute:.width, relatedBy: .equal, toItem:nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 236))
        
        //注册按钮适配
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .left, relatedBy: .equal, toItem: tipLabel, attribute: .left, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .top, relatedBy: .equal, toItem: tipLabel, attribute: .bottom, multiplier: 1.0, constant: 30))
        addConstraint(NSLayoutConstraint(item: registerBtn,
                                         attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        //登录按钮适配
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .right, relatedBy: .equal, toItem: tipLabel, attribute: .right, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .top, relatedBy: .equal, toItem: tipLabel, attribute: .bottom, multiplier: 1.0, constant: 30))
        addConstraint(NSLayoutConstraint(item: loginBtn,
                                         attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        //遮罩层
        //定义VFL中的控件名称和实际名称映射关系
        let viewDict = ["maskIconView":maskIconView,
                        "registerBtn":registerBtn] as [String : Any]
        //横向
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[maskIconView]-0-|", options: [], metrics: nil, views: viewDict))
        //负值要加（）
        //metrics 定义VFL中名称控件和实际名称的映射关系
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[maskIconView]-(-30)-[registerBtn]", options: [], metrics: nil, views: viewDict))
        
        tipLabel.textAlignment = .center

    }
}
