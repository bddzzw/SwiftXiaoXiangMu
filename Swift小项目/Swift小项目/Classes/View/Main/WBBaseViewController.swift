//
//  WBBaseViewController.swift
//  Swift小项目
//
//  Created by bddzzw on 2017/10/26.
//  Copyright © 2017年 bddzzw. All rights reserved.
//

import UIKit


/// Mark 类方法
class WBBaseViewController: UIViewController {
    
    
    //定义字典 访客视图的字典
    var visitorInfor : [String:String]?
    
    
    
    /// 自定义导航条
//    lazy var navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.cz_screenWidth(), height: 64))
//
//    /// 自定义导航条目
//    lazy var navItem = UINavigationItem()
    
    var myTableView : UITableView?
    
    var isPullup = false
    
    //设置是否登录
    var isLogin  = true
    
    //设置访问视图
    var visitorView : WBVisitorView?
    
    
    //系统的刷新控件
    var refreshControl: UIRefreshControl?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadData()
        
        //取消自动缩进 如果隐藏了导航栏 就会缩进20个点
//        automaticallyAdjustsScrollViewInsets = false
        
        // Do any additional setup after loading the view.
    }
    @objc func loadData ()  {
        refreshControl?.endRefreshing()
    }
    /// 从写title的didSet方法
    
//    override var title: String?{
//        didSet{
//            navItem.title=title
//        }
//    }
    // MARK: - 设置界面
    
    private func setupUI(){
        view.backgroundColor=UIColor.cz_random()
       
        setupNavigationBar()
        isLogin ? setupMyTableView() : setupCustomView()
        
        
//        navigationBar.items = [navItem]
    }
    
    private func setupNavigationBar(){
        //加导航栏
//        view.addSubview(navigationBar)
        //设置item的bar
//            navigationBar.items=[navItem]
        //     设置navBar 的渲染颜色
        navigationController?.navigationBar.barTintColor=UIColor.cz_color(withHex: 0xF6F6F6)
        //导航栏的渲染颜色
//        navigationController?.navigationBar.barTintColor=UIColor.cz_color(withHex: 0xF6F6F6)
        //导航栏字体颜
        navigationController?.navigationBar.titleTextAttributes=[NSAttributedStringKey.foregroundColor:UIColor.darkGray]
        //这只系统按钮的文字渲染颜
        navigationController?.navigationBar.tintColor=UIColor.orange
        
    }
    func setupMyTableView(){
        
        myTableView=UITableView(frame:view.bounds , style: .plain)
        view.addSubview(myTableView!)
        myTableView?.delegate=self
        myTableView?.dataSource=self
        
        //实例化刷新控件
        refreshControl=UIRefreshControl()
        //添加到表格视图
        myTableView?.addSubview(refreshControl!)
        //设置刷新事件
        refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        
        
        //设置内容的缩进方法
        // MARK: - 设置内容的缩进
//                myTableView?.contentInset=UIEdgeInsets(top:(navigationController?.navigationBar.bounds.height)!, left: 0, bottom: 0, right: 0)
    }
    //未登录的时候的显示
    // MARK:------访客模式
     private func setupCustomView() {
        visitorView = WBVisitorView(frame: view.bounds)
        
        //        visitorView?.backgroundColor=UIColor.white
        
        view.addSubview(visitorView!)
        
        
        
        visitorView?.visitorInfo=visitorInfor
        
        visitorView?.loginBtn .addTarget(self, action: #selector(loginClick), for: .touchUpInside)
        
        visitorView?.registerBtn .addTarget(self, action: #selector(registorClick), for: .touchUpInside)
        
        navigationItem.leftBarButtonItem=UIBarButtonItem(title: "注册", fontSize: 15, target: self, action: #selector(registorClick))
        navigationItem.rightBarButtonItem=UIBarButtonItem(title: "登录", fontSize: 15, target: self, action: #selector(loginClick))
    }
    


}
//MARK:-访客视图登录注册的坚挺方法
extension WBBaseViewController{
    @objc func loginClick() {
        print("登录")
    }
    @objc func registorClick() {
        print("注册")
    }
}




//-------MARk -----方法的准备 子类中只需重写
// MARK: - 基类只准备方法  子类负责实现 Swift 中的extension 将代码分类管理 便于维护
extension WBBaseViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let row  = indexPath.row
        let section = tableView.numberOfSections - 1
        if row < 0 || section < 0{
            return
        }
        
        let count  = tableView.numberOfRows(inSection: section)
        
        if row == (count - 1) && !isPullup {
            print("上拉加载")
            isPullup = true
            loadData()
        }
        
        
        
        
    }
}
