//
//  WBDemoViewController.swift
//  Swift小项目
//
//  Created by bddzzw on 2017/10/27.
//  Copyright © 2017年 bddzzw. All rights reserved.
//

import UIKit

class WBDemoViewController: WBBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
      
    }
    @objc func NavigaRightBtnClick() {
        navigationController?.pushViewController(WBDemoViewController(), animated: true)
    }
 
    
    
    //重写父类的setupMyTableView方法
    override func setupMyTableView() {
        super.setupMyTableView()
        navigationItem.title="第\(String(describing: navigationController?.childViewControllers.count ?? 0))层"
        navigationItem.rightBarButtonItem=UIBarButtonItem(title: "下一个", fontSize: 16, target: self, action: #selector(NavigaRightBtnClick))
    }
   
}

